# README

Angular2 (Node@7.2.0 + angular-cli@1.7.4) 環境Dockerfile

### build

```
docker build -t arimitsu/angular-cli:1.7.4 .
```

### run

* Shell
```
docker run -it -v `pwd`:/home/node/app arimitsu/angular-cli:1.7.4 /bin/bash
```

* ng serve
```
docker run -it -v `pwd`:/home/node/app -p 4200:4200 -p 49153:49153 arimitsu/angular-cli:1.7.4 ng serve --host 0.0.0.0
```
